/**
 * 用户
 *
 * @export
 * @interface User
 */
export interface User {

    /**
     * password
     *
     * @returns {*}
     * @memberof User
     */
    password?: any;

    /**
     * address
     *
     * @returns {*}
     * @memberof User
     */
    address?: any;

    /**
     * weixin
     *
     * @returns {*}
     * @memberof User
     */
    weixin?: any;

    /**
     * dingding
     *
     * @returns {*}
     * @memberof User
     */
    dingding?: any;

    /**
     * fails
     *
     * @returns {*}
     * @memberof User
     */
    fails?: any;

    /**
     * slack
     *
     * @returns {*}
     * @memberof User
     */
    slack?: any;

    /**
     * ranzhi
     *
     * @returns {*}
     * @memberof User
     */
    ranzhi?: any;

    /**
     * account
     *
     * @returns {*}
     * @memberof User
     */
    account?: any;

    /**
     * locked
     *
     * @returns {*}
     * @memberof User
     */
    locked?: any;

    /**
     * avatar
     *
     * @returns {*}
     * @memberof User
     */
    avatar?: any;

    /**
     * scoreLevel
     *
     * @returns {*}
     * @memberof User
     */
    scorelevel?: any;

    /**
     * realname
     *
     * @returns {*}
     * @memberof User
     */
    realname?: any;

    /**
     * zipcode
     *
     * @returns {*}
     * @memberof User
     */
    zipcode?: any;

    /**
     * dept
     *
     * @returns {*}
     * @memberof User
     */
    dept?: any;

    /**
     * commiter
     *
     * @returns {*}
     * @memberof User
     */
    commiter?: any;

    /**
     * role
     *
     * @returns {*}
     * @memberof User
     */
    role?: any;

    /**
     * 逻辑删除标志
     *
     * @returns {*}
     * @memberof User
     */
    deleted?: any;

    /**
     * last
     *
     * @returns {*}
     * @memberof User
     */
    last?: any;

    /**
     * clientStatus
     *
     * @returns {*}
     * @memberof User
     */
    clientstatus?: any;

    /**
     * skype
     *
     * @returns {*}
     * @memberof User
     */
    skype?: any;

    /**
     * whatsapp
     *
     * @returns {*}
     * @memberof User
     */
    whatsapp?: any;

    /**
     * score
     *
     * @returns {*}
     * @memberof User
     */
    score?: any;

    /**
     * gender
     *
     * @returns {*}
     * @memberof User
     */
    gender?: any;

    /**
     * mobile
     *
     * @returns {*}
     * @memberof User
     */
    mobile?: any;

    /**
     * clientLang
     *
     * @returns {*}
     * @memberof User
     */
    clientlang?: any;

    /**
     * visits
     *
     * @returns {*}
     * @memberof User
     */
    visits?: any;

    /**
     * join
     *
     * @returns {*}
     * @memberof User
     */
    join?: any;

    /**
     * email
     *
     * @returns {*}
     * @memberof User
     */
    email?: any;

    /**
     * ip
     *
     * @returns {*}
     * @memberof User
     */
    ip?: any;

    /**
     * birthday
     *
     * @returns {*}
     * @memberof User
     */
    birthday?: any;

    /**
     * nickname
     *
     * @returns {*}
     * @memberof User
     */
    nickname?: any;

    /**
     * phone
     *
     * @returns {*}
     * @memberof User
     */
    phone?: any;

    /**
     * id
     *
     * @returns {*}
     * @memberof User
     */
    id?: any;

    /**
     * qq
     *
     * @returns {*}
     * @memberof User
     */
    qq?: any;
}