
export default {
  fields: {
    id: '虚拟主键',
    productname: '产品',
    projectname: '项目',
    planname: '计划名称',
    product: '产品',
    plan: '产品计划',
    branch: '平台/分支',
    project: '项目',
  },
	views: {
		planlistview9: {
			caption: "关联计划",
      title: '关联计划',
		},
		listview9: {
			caption: "关联产品",
      title: '关联产品',
		},
	},
};