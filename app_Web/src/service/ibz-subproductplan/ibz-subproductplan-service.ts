import { Http,Util } from '@/utils';
import IBZ_SUBPRODUCTPLANServiceBase from './ibz-subproductplan-service-base';


/**
 * 产品计划服务对象
 *
 * @export
 * @class IBZ_SUBPRODUCTPLANService
 * @extends {IBZ_SUBPRODUCTPLANServiceBase}
 */
export default class IBZ_SUBPRODUCTPLANService extends IBZ_SUBPRODUCTPLANServiceBase {

    /**
     * Creates an instance of  IBZ_SUBPRODUCTPLANService.
     * 
     * @param {*} [opts={}]
     * @memberof  IBZ_SUBPRODUCTPLANService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}