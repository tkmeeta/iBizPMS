import IBZ_SUBPRODUCTPLANUIServiceBase from './ibz-subproductplan-ui-service-base';

/**
 * 产品计划UI服务对象
 *
 * @export
 * @class IBZ_SUBPRODUCTPLANUIService
 */
export default class IBZ_SUBPRODUCTPLANUIService extends IBZ_SUBPRODUCTPLANUIServiceBase {

    /**
     * Creates an instance of  IBZ_SUBPRODUCTPLANUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  IBZ_SUBPRODUCTPLANUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}