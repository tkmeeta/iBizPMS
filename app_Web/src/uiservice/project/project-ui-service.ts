import ProjectUIServiceBase from './project-ui-service-base';

/**
 * 项目UI服务对象
 *
 * @export
 * @class ProjectUIService
 */
export default class ProjectUIService extends ProjectUIServiceBase {

    /**
     * Creates an instance of  ProjectUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  ProjectUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}