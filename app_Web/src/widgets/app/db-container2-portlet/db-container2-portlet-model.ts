/**
 * Db_container2 部件模型
 *
 * @export
 * @class Db_container2Model
 */
export default class Db_container2Model {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof Db_container2Model
    */
  public getDataItems(): any[] {
    return [
    ]
  }


}