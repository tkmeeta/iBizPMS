/**
 * Db_container3 部件模型
 *
 * @export
 * @class Db_container3Model
 */
export default class Db_container3Model {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof Db_container3Model
    */
  public getDataItems(): any[] {
    return [
    ]
  }


}