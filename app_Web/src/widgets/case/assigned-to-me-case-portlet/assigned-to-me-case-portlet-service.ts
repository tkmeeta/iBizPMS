import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * AssignedToMeCase 部件服务对象
 *
 * @export
 * @class AssignedToMeCaseService
 */
export default class AssignedToMeCaseService extends ControlService {
}