/**
 * ProjectExpView 部件模型
 *
 * @export
 * @class ProjectExpViewModel
 */
export default class ProjectExpViewModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof ProjectExpViewModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'openedversion',
      },
      {
        name: 'begin',
      },
      {
        name: 'acl',
      },
      {
        name: 'deleted',
      },
      {
        name: 'desc',
      },
      {
        name: 'pm',
      },
      {
        name: 'project',
        prop: 'id',
      },
      {
        name: 'name',
      },
      {
        name: 'substatus',
      },
      {
        name: 'order',
      },
      {
        name: 'rd',
      },
      {
        name: 'whitelist',
      },
      {
        name: 'pri',
      },
      {
        name: 'end',
      },
      {
        name: 'canceleddate',
      },
      {
        name: 'code',
      },
      {
        name: 'catid',
      },
      {
        name: 'statge',
      },
      {
        name: 'canceledby',
      },
      {
        name: 'iscat',
      },
      {
        name: 'openeddate',
      },
      {
        name: 'closedby',
      },
      {
        name: 'type',
      },
      {
        name: 'po',
      },
      {
        name: 'status',
      },
      {
        name: 'days',
      },
      {
        name: 'team',
      },
      {
        name: 'closeddate',
      },
      {
        name: 'openedby',
      },
      {
        name: 'qd',
      },
      {
        name: 'parentname',
      },
      {
        name: 'parent',
      },
    ]
  }


}