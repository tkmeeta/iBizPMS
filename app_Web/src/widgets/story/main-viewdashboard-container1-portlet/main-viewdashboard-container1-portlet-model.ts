/**
 * MainViewdashboard_container1 部件模型
 *
 * @export
 * @class MainViewdashboard_container1Model
 */
export default class MainViewdashboard_container1Model {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof MainViewdashboard_container1Model
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'assignedto',
      },
      {
        name: 'childstories',
      },
      {
        name: 'plan',
      },
      {
        name: 'version',
      },
      {
        name: 'assigneddate',
      },
      {
        name: 'pri',
      },
      {
        name: 'linkstories',
      },
      {
        name: 'status',
      },
      {
        name: 'estimate',
      },
      {
        name: 'revieweddate',
      },
      {
        name: 'title',
      },
      {
        name: 'sourcenote',
      },
      {
        name: 'reviewedby',
      },
      {
        name: 'substatus',
      },
      {
        name: 'stagedby',
      },
      {
        name: 'openedby',
      },
      {
        name: 'openeddate',
      },
      {
        name: 'story',
        prop: 'id',
      },
      {
        name: 'source',
      },
      {
        name: 'closedreason',
      },
      {
        name: 'color',
      },
      {
        name: 'mailto',
      },
      {
        name: 'deleted',
      },
      {
        name: 'keywords',
      },
      {
        name: 'lasteditedby',
      },
      {
        name: 'stage',
      },
      {
        name: 'closeddate',
      },
      {
        name: 'closedby',
      },
      {
        name: 'type',
      },
      {
        name: 'lastediteddate',
      },
      {
        name: 'path',
      },
      {
        name: 'parentname',
      },
      {
        name: 'modulename',
      },
      {
        name: 'productname',
      },
      {
        name: 'frombug',
      },
      {
        name: 'parent',
      },
      {
        name: 'module',
      },
      {
        name: 'product',
      },
      {
        name: 'duplicatestory',
      },
      {
        name: 'branch',
      },
      {
        name: 'tobug',
      },
      {
        name: 'spec',
      },
      {
        name: 'verify',
      },
    ]
  }


}